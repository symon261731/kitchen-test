/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
const assignObject = (val) => {
  const another = {};
  const values = Object.values(val);
  if (values.some((el) => typeof el === 'object')) {
    values.forEach((el) => {
      if (typeof el === 'object') {
        for (const i in el) {
          another[i] = el[i];
        }
      }
    });
    const result = Object.assign(another, val);
    return result;
  }
  return val;
};

export default assignObject;

import MainPage from '@/pages/MainPage.vue';
import ManClothing from '@/pages/ManClothing.vue';
import JaweleryPage from '@/pages/JaweleryPage.vue';
import ImageStyle from '@/pages/ImageStyle.vue';
import VuexLearn from '@/pages/VuexLearn.vue';
import ErrorPage from '@/pages/ErrorPage.vue';
import { createRouter, createWebHistory } from 'vue-router';

const routes = [
  {
    path: '/',
    component: MainPage,
  },
  {
    path: '/mens-clothing',
    component: ManClothing,
  },
  {
    path: '/jawelery',
    component: JaweleryPage,
  },
  {
    path: '/vuex',
    component: VuexLearn,
  },
  {
    path: '/imageStyle',
    component: ImageStyle,
  },
  {
    path: '/404',
    component: ErrorPage,
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/404',
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;

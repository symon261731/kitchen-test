import axios from 'axios';
import { createStore } from 'vuex';

const store = createStore({
  state: {
    data: [],
    count: 1,
  },
  getters: {
    resultData(state) {
      return state.data;
    },
    doubleCount(state) {
      return state.count * 2;
    },
  },
  mutations: {
    SET_DATA: (state, dataFromAction) => { state.data = dataFromAction; },

  },
  actions: {
    async getDataFromApi(context) {
      const responce = await axios.get('https://api.escuelajs.co/api/v1/categories/');
      context.commit('SET_DATA', responce.data);
    },
  },
});

export default store;
